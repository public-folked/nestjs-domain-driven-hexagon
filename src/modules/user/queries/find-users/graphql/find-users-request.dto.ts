import {
  IsAlphanumeric,
  IsEmail, IsOptional,
  IsString,
  Matches,
  MaxLength,
  MinLength
} from "class-validator";
import { ArgsType, Field, InputType } from "@nestjs/graphql";


@ArgsType()
@InputType()
export class FindUsersGqlRequestDto {
  @IsOptional()
  @MaxLength(320)
  @MinLength(5)
  @IsEmail()
  @Field({ nullable: true })
  readonly email?: string;

  @IsOptional()
  @MaxLength(50)
  @MinLength(4)
  @IsString()
  @Matches(/^[a-zA-Z ]*$/)
  @Field({nullable: true})
  readonly country?: string;

  @IsOptional()
  @MaxLength(10)
  @MinLength(4)
  @IsAlphanumeric()
  @Field({ nullable: true })
  readonly postalCode?: string;

  @IsOptional()
  @MaxLength(50)
  @MinLength(5)
  @Matches(/^[a-zA-Z ]*$/)
  @Field({ nullable: true })
  readonly street?: string;
}
